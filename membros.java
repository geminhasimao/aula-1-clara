package exercicio1Resolucao;

public class membros {
	private String Nome;
	private String Idade;
	private String Segmento;
	private String TempoEquipe;
	
	public void setName(String Nome) {
		this.Nome = Nome;		// perguntar pro professor sobre a utiliza��o das variaveis globais e locais, pq � tudo igual
	}
	
	public String getName() {
		return this.Nome;	
	}
	
	public void setIdade(String Idade) {
		this.Idade = Idade;		
	}
	
	public String getIdade() {
		return this.Idade;
	}
	
	public void setSegmento(String Segmento) {
		this.Segmento = Segmento;
	}

	public String getSegmento() {
		return this.Segmento;
	}
	
	public void setTempoEquipe(String TempoEquipe) {
		this.TempoEquipe = TempoEquipe;
	}
	
	public String getTempoEquipe() {
		return this.TempoEquipe;
	}
	
	public void confirmar() {
		System.out.println("O nome do membro � " + this.getName());
		System.out.println("A idade do membro �  " + this.getIdade());
		System.out.println("O segmento � a " + this.getSegmento());
		System.out.println("O tempo na equipe � de " + this.getTempoEquipe());
	}
}
